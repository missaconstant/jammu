<?php
	header("Access-Control-Allow-Origin: *");
	define("ROOT", __DIR__);

	require_once 'jammu.conf';

	$jammu = new Jammu();

	// L'application renifle tes SMS et envoi les nouveaux sms au serveur

	if (isset($_POST['address'])) {
		$js = json_decode(file_get_contents('messages.json'), true);
		if (JammuI::messageExists($js, $_POST)) $js[] = $_POST;
		// saving for watcher
		file_put_contents('messages.json' ,json_encode($js));
		// fire on message event
		$jammu->onMessage((object) $_POST);
	}

	else if (isset($_GET['get2send'])) {
		// getting messages
		$js = json_decode(file_get_contents('tosend.json'), true);
		$go = json_encode($js['waiting']);
		// emptying message to send list
		$js['waiting'] = [];
		$js['lastquery'] = date('d/m/Y H:i:s');
		file_put_contents('tosend.json', json_encode($js));
		// returning messages
		echo $go;
	}

	else if (isset($_GET['searchserver'])) {
		echo json_encode(["error" => false]);
	}

	else {
		if (strtolower(php_sapi_name()) != 'cli') {
			echo json_encode($_POST);
		}
	}